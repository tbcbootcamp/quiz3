package com.example.shemajamebeli3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shemajamebeli3.HttpRequest.COMPETITIONS
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val competition = Competition()
    private val listOfCompetitions = arrayListOf<AreaModel>()
    private var adapter = CompetitionAdapter(listOfCompetitions, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        loadingTextView.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        request(COMPETITIONS)

        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }
    }

    private fun refresh() {
        swipeRefreshLayout.isRefreshing = true
        listOfCompetitions.clear()
        adapter.notifyDataSetChanged()
        loadingTextView.visibility = View.VISIBLE
        request(COMPETITIONS)
    }

    private fun request(path: String) {
        HttpRequest.onGetRequest(path, object : CustomCallback {
            override fun onResponse(response: String) {
                parseJson(response)
                println(response)
            }

            override fun onFailure(body: Throwable) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

        })
    }

    private fun parseJson(response: String) {
        val json = JSONObject(response)
        if (json.has("count")) competition.count = json.getInt("count")
        if (json.has("filters")) competition.filters = json.getString("filters")
        if (json.has("competitions")) {
            val competitions = json.getJSONArray("competitions")
            for (i in 0 until competitions.length()) {
                val mObject = competitions.getJSONObject(i)
                if (mObject.has("area")) {
                    val modelArea = mObject.getJSONObject("area")
                    val areaModel = AreaModel()
                    if (!modelArea.isNull("id")) areaModel.id = modelArea.getInt("id")
                    if (!modelArea.isNull("name")) areaModel.name = modelArea.getString("name")
                    if (!modelArea.isNull("countryCode")) areaModel.countryCode =
                        modelArea.getString("countryCode")
                    if (!modelArea.isNull("ensignUrl")) areaModel.ensignUrl =
                        modelArea.getString("ensignUrl")
                    listOfCompetitions.add(areaModel)
                }
            }
        }

        recyclerView.adapter = adapter
        loadingTextView.visibility = View.GONE
        swipeRefreshLayout.isRefreshing = false
        adapter.notifyDataSetChanged()
    }
}
