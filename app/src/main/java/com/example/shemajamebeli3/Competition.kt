package com.example.shemajamebeli3


class Competition {
    var count: Int? = 0
    var filters: String? = ""
    var competitions = arrayListOf<CompetitionModel>()
}

class CompetitionModel {
    var id: Int? = 0
    var area = arrayListOf<AreaModel>()
    var name: String? = ""
    var code: String? = null
    var emblemUrl: String? = null
    var plan: String? = ""
    var currentSeason = arrayListOf<CurrentSeasonModel>()
    var numberOfAvailableSeasons: Int? = 0
    var lastUpdated: String? = ""
}


class AreaModel {
    var id: Int? = 0
    var name: String? = ""
    var countryCode: String? = ""
    var ensignUrl: String? = ""

}

class CurrentSeasonModel {
    var id: Int? = 0
    var startDate: Int? = 0
    var endDate: Int? = 0
    var currentMatchday: String? = ""
    var winner: String? = ""
}