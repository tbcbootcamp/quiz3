package com.example.shemajamebeli3

interface CustomCallback {
    fun onFailure(body: Throwable)
    fun onResponse(body: String)
}