package com.example.shemajamebeli3

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.area_recyclerview.view.*

class CompetitionAdapter(private val items: ArrayList<AreaModel>, private val context: Context) :
    RecyclerView.Adapter<CompetitionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.area_recyclerview, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val competitionModel = items[adapterPosition] as AreaModel
            itemView.idTextView.text = "ID: ${competitionModel.id}"
            itemView.nameTextView.text = "Name: ${competitionModel.name}"
            itemView.codeTextView.text = "Code: ${competitionModel.countryCode}"
            if (competitionModel.ensignUrl != null) GlideToVectorYou.justLoadImage(
                context as Activity,
                Uri.parse(competitionModel.ensignUrl),
                itemView.imageView
            )
            else itemView.imageView.setImageResource(R.mipmap.ic_launcher)
        }
    }

}