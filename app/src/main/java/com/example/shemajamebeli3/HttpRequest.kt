package com.example.shemajamebeli3

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object HttpRequest {

    private const val BASE_URL = "https://api.football-data.org/v2/"
    const val COMPETITIONS = "competitions"
    var retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

    private var service: NetServ = retrofit.create(NetServ::class.java)

    interface NetServ{
        @GET("{path}")
        fun getRequest(@Path("path") user: String?): Call<String?>
    }

    fun onGetRequest(path : String, callback : CustomCallback){
        service.getRequest(path).enqueue(object : Callback<String?>{
            override fun onFailure(call: Call<String?>, t: Throwable) {
                callback.onFailure(t)
            }

            override fun onResponse(call: Call<String?>, response: Response<String?>) {
                callback.onResponse(response.body().toString())
            }

        })
    }
}